
import math
import pygame
from PPlay.keyboard import *
from PPlay.window import *
from PPlay.gameimage import *
from PPlay.sprite import *
from PPlay.mouse import *
import os

class Winner:
    def __init__(self, window):
        self.run_ = True
        self.bg = GameImage("assets/winbg.png")
        self.janela = window
        self.winb = Sprite("assets/winb.png")
        self.winb.x = (self.janela.width/2)-(self.winb.width/2)
        self.winb.y = -500
        self.startvel = 800

        self.mouse = Window.get_mouse()


    def run(self):
        while self.run_:
            # faz animação de mover o botão e para
            if self.winb.y >= (self.janela.height / 2 - self.winb.height / 2):
                self.winb.y = self.janela.height / 2 - self.winb.height / 2
            else:
                self.winb.y = self.winb.y + self.startvel * self.janela.delta_time()
            # input mouse: começa o jogo
            if self.mouse.is_over_object(self.winb) and self.mouse.is_button_pressed(1):
                return "m"

            self.draw()


    def draw(self):
        # draw functions
        self.bg.draw()
        self.winb.draw()
        self.janela.update()