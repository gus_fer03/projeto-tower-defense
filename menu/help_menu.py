import pygame
from PPlay.window import *
from PPlay.gameimage import *
from PPlay.sprite import *
import os

ratio = 0.7
class TowerSprite:
    def __init__(self, x, y, img):
        self.x = x
        self.y = y

        self.img = pygame.transform.scale(img, ((img.get_width()*ratio), (img.get_height()*ratio)))

        self.width = self.img.get_width()
        self.height = self.img.get_height()

class HelpMenu:
    def __init__(self, window, keyboard):
        self.help_sprite = Sprite("assets/container-help.png")
        self.help_sprite.x = window.width/2 - self.help_sprite.width/2
        self.help_sprite.y = window.height

        self.keyboard = keyboard
        self.mouse = window.get_mouse()
        self.window = window
        self.pygameWindow = self.window.get_screen()
        self.key_cooldown = 0.2

        self.tow_list = [TowerSprite(0, 0, pygame.image.load("assets/branco/branco-peao.png")), TowerSprite(0, 0, pygame.image.load("assets/branco/branco-bispo.png")), 
                        TowerSprite(0, 0, pygame.image.load("assets/branco/branco-cavalo.png")), TowerSprite(0, 0, pygame.image.load("assets/branco/branco-torre.png")),
                        TowerSprite(0, 0, pygame.image.load("assets/branco/branco-rainha.png"))]
        temp_x = self.help_sprite.x + 35

        for tow in self.tow_list:
            tow.y = self.help_sprite.y + 60
            tow.x = temp_x + 45 - tow.width/2
            temp_x = temp_x + 110

        self.help_open = False

    def helpme(self):
        clock = self.window.delta_time()
        self.key_cooldown -= clock

        #tudo que se trata do menu de ajuda: subir, descer, abrir e fechar.
        if (self.keyboard.key_pressed("h") and self.key_cooldown <= 0) and not(self.help_open):
            self.help_open = True
            self.key_cooldown = 0.2
        if self.help_open and (self.keyboard.key_pressed("h") and self.key_cooldown <= 0):
            self.help_open = False
            self.key_cooldown = 0.2

        if self.help_sprite.y >= (self.window.height - self.help_sprite.height) and self.help_open:
            self.help_sprite.y = self.help_sprite.y - 800 * clock
            for tow in self.tow_list:
                tow.y = tow.y - 800 * clock
        elif self.help_sprite.y <= (self.window.height + 10) and not self.help_open:
            self.help_sprite.y = self.help_sprite.y + 800 * clock
            for tow in self.tow_list:
                tow.y = tow.y + 800 * clock

    def clicked(self):
        card_areas = []
        for i in range(5):
            card_areas.append(((self.help_sprite.x+35+(110*i), self.help_sprite.y+20), (self.help_sprite.x+35+(110*i)+90, self.help_sprite.y+130)))

        for card in card_areas:
            if self.mouse.is_over_area(*card) and self.mouse.is_button_pressed(1):
                self.help_open = False
                print(card_areas.index(card)+1)
                return card_areas.index(card)+1
        return False

    def draw(self):
        self.help_sprite.draw()
        for tow in self.tow_list:
            self.pygameWindow.blit(tow.img, (tow.x, (tow.y-tow.height/2)))
