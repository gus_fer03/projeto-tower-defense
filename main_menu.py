import math
import pygame
from PPlay.keyboard import *
from PPlay.window import *
from PPlay.gameimage import *
from PPlay.sprite import *
from PPlay.mouse import *
import os
import game
class Menu:
    def __init__(self,window):
        self.run_ = True
        self.bg = GameImage("assets/menu.png")
        self.janela = window
        self.startb = Sprite("assets/jogar.png")
        self.startb.y = 300
        self.startb.x =   self.janela.width/2 - self.startb.width/2
        self.startvel = 800

        self.mouse = Window.get_mouse()

    def run(self):
        while self.run_:

            #input mouse: começa o jogo
            if self.mouse.is_over_object(self.startb) and self.mouse.is_button_pressed(1):
                return "g"

            self.draw()

    def draw(self):

            #draw functions
            self.bg.draw()
            self.startb.draw()
            self.janela.update()

# Instancia o jogo (jame é para não sobrescrever o 'import game')
#menu_ = Menu()
#menu_.run()



