import pygame
import math

class Tower:
    def __init__(self, x, y, imgs):
        self.imgs = imgs
        self.img_count = 0
        self.img = self.imgs[self.img_count]

        self.width = self.img.get_width()
        self.height = self.img.get_height()

        self.x = x
        self.y = y

        self.cooldown = 0
        self.current_cooldown = self.cooldown

        self.damage = 0
        self.range = 0
        self.inRange = False
        self.attacking = False
        self.right = False
        self.moving = False

        self.place_color = (0,0,255, 100)

    def draw(self, window):
        """
        Desenha o inimigo de acordo com as imagens
        :param win: surface
        :return: None
        """
        if self.moving:
            self.x, self.y = pygame.mouse.get_pos()
        else:
            if self.inRange and self.attacking:
                self.img_count += 1
                if self.img_count >= len(self.imgs) * 10:
                    self.img_count = 0
            else:
                self.img_count = 0

        self.img = self.imgs[self.img_count // 10]
        window.blit(self.img, (self.x - self.width / 2, self.y - self.height))

    def attack(self, enemies, clock):
        """
        Ataca o inimigo
        :param enemies: list
        :return: int (dinheiro)
        """

        # Diminui o cooldown
        self.current_cooldown -= clock.get_time()/1000
        # Checa se a torre está em cooldown

        money = 0
        self.inRange = False
        enemy_in_range = []

        for enemy in enemies:
            x = enemy.x
            y = enemy.y

            dis = math.sqrt((self.x - enemy.img.get_width()/2 - x)**2 + (self.y -enemy.img.get_height()/2 - y)**2)
            if dis < self.range:
                self.inRange = True
                enemy_in_range.append(enemy)

        # Organiza a lista em relação a distância do inimigo ao rei
        enemy_in_range.sort(key=lambda x: x.path_pos)
        enemy_in_range = enemy_in_range[::-1]

        # Checa se há algum inimigo no range e a torre não está em cooldown
        if enemy_in_range:
            first_enemy = enemy_in_range[0]

            if self.img_count == 0 and self.current_cooldown <= 0:
                self.attacking = True

            if self.img_count == (len(self.imgs)*10)-1 and self.attacking:
                # Reseta o cooldown
                self.current_cooldown = self.cooldown

                # Checa se o inimigo morreu
                if first_enemy.hit(self.damage) == True:
                    money = first_enemy.money
                    enemies.remove(first_enemy)
                self.attacking = False

            if first_enemy.x > self.x and not(self.right):
                self.right = True
                for x, img in enumerate(self.imgs):
                    self.imgs[x] = pygame.transform.flip(img, True, False)
            elif self.right and first_enemy.x < self.x:
                self.right = False
                for x, img in enumerate(self.imgs):
                    self.imgs[x] = pygame.transform.flip(img, True, False)
        return money

    def move(self, x, y):
        """
        moves tower to given x and y
        :param x: int
        :param y: int
        :return: None
        """
        self.x = x
        self.y = y

    def collided(self, otherTower):
        x2 = otherTower.x
        y2 = otherTower.y

        dis = math.sqrt((x2 - self.x)**2 + (y2 - self.y)**2)
        if dis >= 100:
            return False
        else:
            return True

    def draw_placement(self, window):
        # Desenha o range
        surface = pygame.Surface((self.range * 4, self.range * 4), pygame.SRCALPHA, 32)
        pygame.draw.circle(surface, self.place_color, (50,50), 50, 0)

        window.blit(surface, (self.x - self.width / 2, self.y - self.height))