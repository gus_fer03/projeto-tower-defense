import pygame
import os
from .tower import Tower

imgs = []
for x in range(5):
    imgs.append(pygame.image.load(os.path.join("assets", "branco", "branco-cavalo", "branco-cavalo-" + str(x) + ".png")))

class WhiteKnight(Tower):
    def __init__(self, x, y):
        super().__init__(x, y, imgs[:])
        self.cooldown = 0.8
        self.damage = 7
        self.range = 150