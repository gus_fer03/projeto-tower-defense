import pygame
import os
from .tower import Tower

class WhiteBishop(Tower):

    def __init__(self, x, y):
        super().__init__(x, y, [pygame.image.load(os.path.join("assets", "branco", "branco-bispo.png"))])
        self.cooldown = 0.6
        self.damage = 3
        self.range = 200