import pygame
import os
from .tower import Tower

imgs = []
for x in range(5):
    imgs.append(pygame.image.load(os.path.join("assets", "branco", "branco-rainha", "branco-rainha-" + str(x) + ".png")))
class WhiteQueen(Tower):

    def __init__(self, x, y):
        super().__init__(x, y, imgs[:])

        self.cooldown = 1
        self.damage = 20
        self.range = 400