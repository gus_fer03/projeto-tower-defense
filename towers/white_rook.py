import pygame
import os
from .tower import Tower

class WhiteRook(Tower):

    def __init__(self, x, y):
        super().__init__(x, y, [pygame.image.load(os.path.join("assets", "branco", "branco-torre.png"))])

        self.cooldown = 4
        self.damage = 60
        self.range = 300