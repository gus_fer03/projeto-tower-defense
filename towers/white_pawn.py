import pygame
import os
from .tower import Tower

class WhitePawn(Tower):

    def __init__(self, x, y):
        super().__init__(x, y, [pygame.image.load(os.path.join("assets", "branco", "branco-peao.png"))])

        self.cooldown = 0.8
        self.damage = 1
        self.range = 130

