import pygame
import os
from .tower import Tower

class WhiteKing(Tower):

    def __init__(self, x, y):
        super().__init__(x, y, [pygame.image.load(os.path.join("assets", "branco", "branco-bispo.png"))])
