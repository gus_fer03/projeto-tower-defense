import math
import pygame
from PPlay.keyboard import *
from PPlay.window import *
from PPlay.gameimage import *
from PPlay.sprite import *
from PPlay.mouse import *
import os

class Loose:
    def __init__(self, window):
        self.run_ = True
        self.bg = GameImage("assets/gover.png")
        self.janela = window
        self.gob = Sprite("assets/retry.png")
        self.gob.x = (self.janela.width/2)-(self.gob.width/2)
        self.gob.y = 350
        self.gob.x = self.janela.width / 2 - self.gob.width / 2

        self.mouse = Window.get_mouse()


    def run(self):
        while self.run_:


            # input mouse: começa o jogo
            if self.mouse.is_over_object(self.gob) and self.mouse.is_button_pressed(1):
                return "g"

            self.draw()


    def draw(self):
        # draw functions
        self.bg.draw()
        self.gob.draw()
        self.janela.update()