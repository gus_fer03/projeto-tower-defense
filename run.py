import math
import pygame
from PPlay.keyboard import *
from PPlay.window import *
from PPlay.gameimage import *
from PPlay.sprite import *
from PPlay.mouse import *
import os
import game
import main_menu
import win
import gameover

class Main:
    def __init__(self):
        self.janela = Window(1067, 600)
        self.telaativa = "m"
        self.menu_ = main_menu.Menu(self.janela)
    def tela(self):
        while True:
            #metodos retornam a tela que querem por
            if self.telaativa == "m":
                self.telaativa = self.menu_.run()
            elif self.telaativa == "g":
                game_ = game.Game(self.janela)
                self. telaativa=  game_.run()
            elif self.telaativa == "w":
                win_ = win.Winner(self.janela)
                self.telaativa = win_.run()
            elif  self.telaativa == "go":
                gameover_ = gameover.Loose(self.janela)
                self.telaativa = gameover_.run()

main = Main()
main.tela()