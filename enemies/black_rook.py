import pygame
import os
from .enemy import Enemy


class BlackRook(Enemy):
    imgs = [pygame.image.load(os.path.join("assets", "preto", "preto-torre.png"))]

    def __init__(self):
        super().__init__()
        self.max_health = 400
        self.health = self.max_health
        self.money = 1500

        self.vel = 15
