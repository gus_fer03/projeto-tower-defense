import pygame
import os
from .enemy import Enemy


class BlackPawn(Enemy):
    imgs = [pygame.image.load(os.path.join("assets", "preto", "preto-peao.png"))]

    def __init__(self):
        super().__init__()
        self.max_health = 4
        self.health = self.max_health
        self.money = 150

        self.vel = 50
