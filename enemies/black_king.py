import pygame
import os
from .enemy import Enemy


class BlackKing(Enemy):
    imgs = [pygame.image.load(os.path.join("assets", "preto", "preto-rei.png"))]

    def __init__(self):
        super().__init__()
        self.max_health = 3000
        self.health = self.max_health
        self.money = 3000

        self.vel = 10
