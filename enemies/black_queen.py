import pygame
import os
from .enemy import Enemy


class BlackQueen(Enemy):
    imgs = [pygame.image.load(os.path.join("assets", "preto", "preto-rainha.png"))]

    def __init__(self):
        super().__init__()
        self.max_health = 500
        self.health = self.max_health
        self.money = 2000

        self.vel = 70
