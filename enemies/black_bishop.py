import pygame
import os
from .enemy import Enemy


class BlackBishop(Enemy):
    imgs = [pygame.image.load(os.path.join("assets", "preto", "preto-bispo.png"))]

    def __init__(self):
        super().__init__()
        self.max_health = 25
        self.health = self.max_health
        self.money = 500

        self.vel = 40
