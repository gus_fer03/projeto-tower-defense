import pygame
import math

# Classe abstrata
class Enemy:
    imgs = []
    vel = 0

    def __init__(self):
        self.img = self.imgs[0]

        self.max_health = 0
        self.money = 0

        self.width = self.img.get_width()
        self.height = self.img.get_height()

        # Velocidade
        self.vel = 0
        # Caminho do inimigo
        self.path = [(-30, 230), (115, 230), (191, 275), (456, 277), (520, 240), (564, 98), (631, 75), (711, 100), (743, 223), (854, 280), (930, 358), (866, 461), (647, 467), (571, 518), (430, 514), (382, 448), (170, 441)]
        self.path_pos = -1

        self.x = self.path[0][0]
        self.y = self.path[0][1]
        
        self.velA = (0, 0)
        self.mov_dist = 0.0
        self.trav_dist = 0.0

        # Caso o inimigo esteja virado pro lado direito
        self.flipped = False
        self.img = pygame.transform.flip(self.img, True, False)
    
        self.finish = False
        
    # Desenha o inimigo na tela
    def draw(self, window, clock):
        """
        Desenha o inimigo de acordo com as imagens
        :param win: surface
        :return: None
        """
        window.blit(self.img, (self.x - self.width / 2, self.y - self.height))
        self.get_health_bar(window)
        self.move(clock)

    def collide(self, x, y):
        """
        Retorna se o inimigo alcançou tal posição
        :param win: surface
        :param x: int
        :param y: int
        :return: Bool
        """

        if self.x <= x <= self.x + self.width:
            if self.y <= y <= self.y + self.height:
                return True
        return False

    def move(self, clock):
        """
        Move o inimigo
        :return: Bool
        """
        
        if ((self.velA[0] == 0) and (self.velA[1] == 0)) or (self.trav_dist > self.mov_dist):
            # se sim gruda no ponto direto, bota o timer no 0 e manda calcular a velocidade pro proximo frame -G
            self.path_pos = self.path_pos + 1

            if self.path_pos < (len(self.path)-1):
                self.x = self.path[self.path_pos][0]
                self.y = self.path[self.path_pos][1]
                self.velA = self.recalcular_vel()

            else:
                self.path_pos = self.path_pos - 1
                self.velA = (0, 0)
                self.finish = True
            self.trav_dist = 0

        else:
            # senão manda calular a distancia e passa pra frente -G
            # dist_a = dist_a + self.vel * time
            self.trav_dist += self.vel*(clock.get_time()/1000)
            self.x += self.velA[0]*(clock.get_time()/1000)
            self.y += self.velA[1]*(clock.get_time()/1000)

        if self.velA[0] < 0 and not(self.flipped):
            self.flipped = True
            self.img = pygame.transform.flip(self.img, True, False)

    def recalcular_vel(self):
        # recalcula a velocidade por eixo
        old_pos = self.path[self.path_pos]
        new_pos = self.path[self.path_pos + 1]

        x_vel = self.vel * (new_pos[0] - old_pos[0]) / (math.dist(old_pos, new_pos))
        y_vel = self.vel * (new_pos[1] - old_pos[1]) / (math.dist(old_pos, new_pos))
        self.mov_dist = math.dist(old_pos, new_pos)

        return (x_vel, y_vel)

    def get_health_bar(self, window):
        """
        Desenha a barra de vida do inimigo
        :param window: janela
        :return: None
        """

        length = 50
        move_by = length / self.max_health
        health_bar = round(move_by * self.health)

        # Vermelho
        pygame.draw.rect(window, (255,0,0), (self.x-25, self.y-self.height-20, length, 10), 0)
        # Verde
        pygame.draw.rect(window, (0, 255, 0), (self.x-25, self.y-self.height-20, health_bar, 10), 0)

    def hit(self, damage):
        """
        Diminui a vida do inimigo e o remove caso tenha morrido
        :return: Bool
        """
        self.health -= damage
        if self.health <= 0:
            return True
