import pygame
from PPlay.window import *
from PPlay.gameimage import *
from PPlay.sprite import *
import os

# import menu
from menu.help_menu import HelpMenu
import Kq

from enemies.black_pawn import BlackPawn
from enemies.black_knight import BlackKnight
from enemies.black_bishop import BlackBishop
from enemies.black_rook import BlackRook
from enemies.black_queen import BlackQueen
from enemies.black_king import BlackKing

from towers.white_pawn import WhitePawn
from towers.white_knight import WhiteKnight
from towers.white_bishop import WhiteBishop
from towers.white_rook import WhiteRook
from towers.white_queen import WhiteQueen
from towers.white_king import WhiteKing

clock = pygame.time.Clock()

waves = [
    [3, 0, 0, 0, 0, 0],
    [4, 0, 0, 0, 0, 0],
    [2, 1, 1, 0, 0, 0],
    [6, 3, 2, 0, 0, 0],
    [5, 3, 2, 1, 0, 0],
    [8, 3, 3, 2, 0, 0],
    [10, 5, 5, 3, 1, 0],
    [12, 7, 7, 5, 1, 1],
    [15, 8, 8, 7, 2, 0],
    [20, 10, 10, 9, 2, 1]
]


def spawnEnemies(pawn, knight, bishop, rook, queen, king):
    new_to_spawn = []

    for i in range(pawn):
        new_to_spawn.append(BlackPawn())
    for i in range(knight):
        new_to_spawn.append(BlackKnight())
    for i in range(bishop):
        new_to_spawn.append(BlackBishop())
    for i in range(rook):
        new_to_spawn.append(BlackRook())
    for i in range(queen):
        new_to_spawn.append(BlackQueen())
    for i in range(king):
        new_to_spawn.append(BlackKing())

    return new_to_spawn


# Cria a classe do jogo
class Game:
    def __init__(self, window):
        # Atributos da classe jogo
        self.window = window
        self.pygameWindow = self.window.get_screen()

        self.to_spawn = []
        self.spawn_cooldown = 0.75

        self.enemies = []
        self.current_wave = 0

        self.towers = []
        self.lives = 10
        self.money = 500
        self.background = GameImage(os.path.join("assets", "mapa.jpg"))

        self.keyboard = self.window.get_keyboard()

        self.moving_object = None
        self.objspawnd = False
        # help system
        self.help_menu = HelpMenu(self.window, self.keyboard)
        self.win = False

        # parte para remoção de tropas
        self.removemodeon = False
        self.key_cooldown = 0

        self.mouse = Window.get_mouse()
        self.lixinho = Sprite("assets/lixo.png")
        self.lixinho.x = -100
        self.lixinho.y = -100

        self.white_king = Sprite("assets/branco/branco-rei.png")
        self.white_king.x = 45
        self.white_king.y = 360

    # Cria a função de rodar o jogo
    def run(self):
        run = True
        # Game Loop
        while run:
            #if self.keyboard.key_pressed("g"):
             #   return "w"
            if self.lives < 0:
                return "go"
            clock.tick()
            self.key_cooldown = self.key_cooldown - clock.get_time()/1000
            # retorno com esc
            if self.keyboard.key_pressed("esc") and self.keyboard.key_pressed('left_shift'):
                return "m"
            # Checa os botões do teclado

            if self.keyboard.key_pressed("e") and self.objspawnd:
                # Checa se a posição é válida
                if ((770 < self.moving_object.x < 830) and (340 < self.moving_object.y < 400)) or (
                        (490 < self.moving_object.x < 550) and (400 < self.moving_object.y < 460)) or (
                        (660 < self.moving_object.x < 720) and (530 < self.moving_object.y < 590)) or (
                        (240 < self.moving_object.x < 300) and (500 < self.moving_object.y < 560)) or (
                        (310 < self.moving_object.x < 370) and (160 < self.moving_object.y < 220)) or (
                        (600 < self.moving_object.x < 660) and (155 < self.moving_object.y < 215)):
                    not_allowed = False
                    for tower in self.towers:
                        if tower.collided(self.moving_object):
                            not_allowed = True
                            break
                    if not (not_allowed):
                        self.moving_object.moving = False
                        self.towers.append(self.moving_object)
                        self.money -= current_tower_price
                        self.objspawnd = False
                        self.moving_object = None

            if self.keyboard.key_pressed("q") and self.objspawnd:
                self.objspawnd = False
                self.moving_object = None

            # Spawna os inimigos da lista to_spawn
            self.spawn_cooldown -= clock.get_time() / 1000
            if self.to_spawn and self.spawn_cooldown <= 0:
                self.enemies.append(self.to_spawn.pop(0))
                self.spawn_cooldown = 0.75

            if not (self.to_spawn) and not (self.enemies):
                self.to_spawn = spawnEnemies(*waves[self.current_wave])
                try:
                    self.current_wave += 1
                except IndexError:
                    self.win = True
                    return "w"

            # help movement
            self.help_menu.helpme()
            if not(self.objspawnd):
                current_tower_price = self.add_tower(self.help_menu.clicked())
            self.removemode()
            self.draw()
            self.window.update()

    def draw(self):
        self.background.draw()

        self.white_king.draw()

        # Mostra a quantidade de vidas
        self.window.draw_text(f"VIDAS: {self.lives}", 20, 10, 30, (255, 255, 255))

        # Mostra o dinheiro
        self.window.draw_text(f"DINHEIRO: {self.money}", self.window.width - 250, 10, 30, (255, 255, 255))

        # Mostra a wave atual
        self.window.draw_text(f"WAVE: {self.current_wave}", 20, self.window.height - 50, 30, (255, 255, 255))

        # Desenha as torres na tela
        for tower in self.towers:
            tower.draw(self.pygameWindow)
            self.money += tower.attack(self.enemies, clock)

        # Desenha os inimigos na tela
        for enemy in self.enemies:
            enemy.draw(self.pygameWindow, clock)

            # Remove o inimigo caso tenha alcançado o rei
            if enemy.finish:
                self.enemies.remove(enemy)
                self.lives -= 1

        if self.moving_object:
            self.moving_object.draw(self.pygameWindow)

        self.help_menu.draw()
        self.lixinho.draw()

    # Adiciona uma torre
    def add_tower(self, number):
        if number:
            x, y = pygame.mouse.get_pos()
            price_list = [200, 700, 800, 2000, 5000]
            tower_list = [WhitePawn(x, y), WhiteBishop(x, y), WhiteKnight(x, y), WhiteRook(x, y), WhiteQueen(x, y)]
            if self.money >= price_list[number-1]:
                obj = tower_list[number-1]
                self.moving_object = obj
                self.objspawnd = True
                obj.moving = True


                return price_list[number-1]

    def removemode(self):

        self.lixinho.x = 20;
        self.lixinho.y = 20
        if (self.keyboard.key_pressed("r") and self.key_cooldown <= 0) and not self.removemodeon:
            self.removemodeon = True
            self.key_cooldown = 0.2

        if self.removemodeon and (self.keyboard.key_pressed("r") and self.key_cooldown <= 0):
            self.removemodeon = False
            self.key_cooldown = 0.2

        if self.removemodeon:
            self.removeclick()
            self.lixinho.x = 20
            self.lixinho.y = 50
        else:
            self.lixinho.x = -100
            self.lixinho.y = -100

    def removeclick(self):
        for item in self.towers:
            if self.mouse.is_over_object(item) and self.mouse.is_button_pressed(1):
                self.towers.remove(item)